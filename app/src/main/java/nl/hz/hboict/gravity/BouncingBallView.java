package nl.hz.hboict.gravity;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.view.View;

import java.util.ArrayList;
import java.util.Formatter;

/**
 * Created by waar0003 on 02-12-2015.
 */
public class BouncingBallView extends View {
    private static final float G = 6.67428E-11f;

    // the moving objects
    private ArrayList<Ball> objects = new ArrayList<Ball>();
    private ArrayList<Collider> colliders = new ArrayList<Collider>();

    private double scale = 1E-6;
    private int xMin = 0;          // This view's bounds
    private int xMax;
    private int yMin = 0;
    private int yMax;

    private Paint paint;           // The paint (e.g. style, color) used for drawing

    private float prevImpulse = 0;
    private float integrated = 0;

    // Status message to show Ball's (x,y) position and speed.
    private StringBuilder statusMsg = new StringBuilder();
    private Formatter formatter = new Formatter(statusMsg);  // Formatting the statusMsg

    // Constructor
    public BouncingBallView(Context context) {
        super(context);
        Ball earth = new Ball("Earth", Color.BLUE, 127f, 5.972E9f,
                new Vector(0, 0),
                new Vector(-0.9f, 0.1f) //-0.03f
        );
        addObject(earth);
        Ball moon = new Ball("Moon", Color.GRAY, 34f, 3.434767309E8f,
                earth.getPosition().add(new Vector(150, 284f)),
                new Vector(16f, -3) //5.3f
        );
        addObject(moon);
        Ball daan = new Ball("Daan", Color.GREEN, 10f, 4.3E4f,
                earth.getPosition().add(new Vector(-127, 10)),
                new Vector(-11, 12.8f)
        );
        addObject(daan);
        paint = new Paint();
        update(1);
        Vector i = new Vector(0,0);
        for(Ball object : objects) {
            i = i.add(object.getImpulse());
        }
        prevImpulse = i.getSize();
    }

    private void addObject(Ball object) {
        for (Ball b : objects) {
            colliders.add(new Collider(object, b));
        }
        objects.add(object);
    }
    // Called back to draw the view. Also called by invalidate().
    @Override
    protected void onDraw(Canvas canvas) {
        // Draw the status message
        paint.setColor(Color.CYAN);
        // text size in pixels
        paint.setTextSize((int) (24));
        int y = 30;
        Vector i = new Vector(0,0);
        for(Ball object : objects) {
            canvas.drawText(object.toString(), 10, y, paint);
            i = i.add(object.getImpulse());
            y +=30;
        }
        float imp = i.getSize();
        float diff = imp-prevImpulse;
        integrated += diff;
        String impulse = "Impulse: " + integrated;
        prevImpulse = imp;
        canvas.drawText(impulse,10,y,paint);
        canvas.translate(getWidth() / 2, getHeight() / 2);
        for(Ball object : objects)
            object.draw(canvas, paint);

        // Update the position of the ball, including collision detection and reaction.
        for (int n=0; n<10; n++)
            update(0.04f);

        // Delay
        try {
            Thread.sleep(1);
        } catch (InterruptedException e) { }

        invalidate();  // Force a re-draw
    }

    // Detect collision and update the position of the ball.
    private void update(float interval) {
        for (Ball o1 : objects) {
            for (Ball o2 : objects) {
                if (!(o1 == o2)) {
                    applyGravity(o1, o2);
                }
            }
        }
        for (Ball o : objects) {
            o.move(interval);
        }
        for (Collider c : colliders) {
            c.checkCollision(interval);
        }
    }

    private void applyGravity(Ball from, Ball to) {
        Vector direction = to.getPosition().sub(from.getPosition());
        float distance = direction.getSize();
        float size = G * from.getMass() * to.getMass() / distance*distance;
        from.addForce(direction.normalize().scale(size));
    }

    private void collide(Ball from, Ball to) {
        Vector direction = to.getPosition().sub(from.getPosition());
        float distance = direction.getSize();
        if (distance < from.getRadius() + to.getRadius()) {
            from.setColliding(true);
            //from.handleCollisionWith(to);
        }
    }
    // Called back when the view is first created or its size changes.
    @Override
    public void onSizeChanged(int w, int h, int oldW, int oldH) {
        // Set the movement bounds for the ball
        xMax = w-1;
        yMax = h-1;

    }
}
