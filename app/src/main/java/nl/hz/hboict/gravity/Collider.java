package nl.hz.hboict.gravity;

/**
 * Created by waar0003 on 02-12-2015.
 */
public class Collider {

    private Ball b1;
    private Ball b2;

    private int wasColliding = 0;

    public Collider(Ball b1, Ball b2) {
        this.b1 = b1;
        this.b2 = b2;
    }

    public void checkCollision(float time) {
        // Create a vector from center of b1 to b2;
        Vector direction = b2.getPosition().sub(b1.getPosition());
        // And calculate the distance between them
        float distance = direction.getSize();
        if (distance < b1.getRadius() + b2.getRadius()) {
            b1.setColliding(true);
            b2.setColliding(true);
            if (wasColliding==0)
                handleCollision(time);
            wasColliding++;
        }
        wasColliding = 0;
    }

    public void handleCollision(float time) {
//        b1.move(-time / 2);
//        b2.move(-time/2);

        //get the e
        float e = 0.3f;
        float m1 = b1.getMass();
        float m2 = b2.getMass();
        float m1p = m1/(m1+m2);
        float m2p = m2/(m1+m2);

        Vector eur = b1.getSpeed().sub(b2.getSpeed()).scale(e);

        Vector v1 = b1.getSpeed().scale(m1p).
                add(b2.getSpeed().sub(eur).scale(m2p));

        b1.setSpeed(v1);
        b2.setSpeed(eur.add(v1));

//        b1.move(time/2);
//        b2.move(time/2);
    }
}
