package nl.hz.hboict.gravity;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;

import java.util.Formatter;


public class Ball {

    private String name;
    private int color;
    private float radius;
    private float mass;
    private Vector force;
    private Vector position;
    private Vector speed;
    private Vector handledCollisionSpeed;
    private RectF ballBounds;      // Needed for Canvas.drawOval

    private boolean colliding=false;

    // Status message to show Ball's (x,y) position and speed.
    private StringBuilder statusMsg = new StringBuilder();
    private Formatter formatter = new Formatter(statusMsg);  // Formatting the statusMsg

    public Ball(String name, int color, float diameter, float mass, Vector position, Vector speed) {
        this.name = name;
        this.color = color;
        this.radius = diameter/2;
        this.mass = mass;
        this.force = new Vector(0,0);
        this.position = position;
        this.speed = speed;
        ballBounds = new RectF();
    }

    public void setColliding(boolean colliding) {
        this.colliding = colliding;
    }

    public float getRadius() {
        return radius;
    }

    public float getMass() {
        return mass;
    }

    public Vector getPosition() {
        return position;
    }

    public void setPosition(Vector position) {
        this.position = position;
    }

    public Vector getSpeed() {
        return speed;
    }

    public void setSpeed(Vector speed) {
        this.speed = speed;
    }

    public Vector getImpulse() {
        return speed.scale(mass);
    }
    public void addForce(Vector force) {
        this.force = this.force.add(force);
    }
    
    
    public void move(float time) {
        // assuming we are not colliding now
        colliding = false;

        // turn the force into acceleration by Newton's first law
        Vector acceleration = this.force.scale(1f/this.mass);
        // the force has done its job: set it to zero
        this.force = new Vector(0,0);

        // formula: S = V0*t + 1/2at^2
        Vector displacement = speed.scale(time);
        position = position.add(displacement);

        // formula: Vt = V0 + a.t
        speed = speed.add(acceleration.scale(time));
        // Build status message
        statusMsg.delete(0, statusMsg.length());   // Empty buffer
        formatter.format("%s@(%1.0f,%1.0f),Speed=(%.2f,%.2f)", name, position.getX(), position.getY(),
                speed.getX(), speed.getY());
    }

    public void draw(Canvas canvas, Paint paint) {
        float ballX = position.getX();
        float ballY = position.getY();
        // Draw the ball
        ballBounds.set(ballX - radius, ballY - radius, ballX + radius, ballY + radius);
        if (colliding)
            paint.setColor(Color.RED);
        else
            paint.setColor(color);
        canvas.drawOval(ballBounds, paint);
    }

    public String toString() {
        return statusMsg.toString();
    }
}