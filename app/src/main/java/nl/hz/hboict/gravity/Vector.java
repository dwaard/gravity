package nl.hz.hboict.gravity;

import java.util.Formatter;

public class Vector {

    private float x;
    private float y;

    public Vector(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }
    
    public Vector scale(float factor) {
        return new Vector( x * factor, y * factor );
    }
    
    public Vector add(Vector other) {
        return new Vector( x  + other.getX(), y + other.getY() );
    }
    
    public Vector sub(Vector other) {
        return new Vector( x  - other.getX(), y - other.getY() );
    }
    
    public float getSize() {
        return (float) Math.sqrt(x*x + y*y);
    }
    
    public Vector normalize() {
        return this.scale(1/this.getSize());
    }

    public Vector tangent() { return new Vector(-y, x); }

    public float dot(Vector v) { return x*v.getX() + y*v.getY(); }

    @Override
    public String toString() {
        return "(" + x + "," + y + ')';
    }
}